flast will be divided into different subgroups based on different aspects of the project.
For now, all projects are in the flast group, since projects inside a subgroup can't have a page.
This is projected to change Q4 of 2019.

For now, all projects shall be named subgroupname-projectname.
All pages will be served at projectname.subgroupname.flast.org .
Except for "main" projects, which will serve as a root and be served at subgroupname.flast.org .

All pages should require https.
The certificates will automatically be renewed by the private SSL project, which will run certbot from Let'sEncrypt on schedule once a month.
Each page will need a specific certificate for it's subdomain, which will require a specific DNS challenge.
This is intended to avoid getting a wildcard certificate leaked, reducing the potential leak to a single subdomain.
This should get streamlined in Q4 of 2019, as gitlab aims to automate Let'sEncrypt certificate renewals.